package com.vilo.repository.di

import com.vilo.repository.AppDispatchers
import com.vilo.repository.ShoppingRepository
import com.vilo.repository.ShoppingRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module.module

val repositoryModule = module {
    factory { AppDispatchers(Dispatchers.Main, Dispatchers.IO) }
    factory { ShoppingRepositoryImpl()
    }
}