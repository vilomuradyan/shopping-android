package com.vilo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product")
data class Product(
    @PrimaryKey
     val name: String,

    @ColumnInfo(name = "brand")
     val brand: String,

    @ColumnInfo(name = "type")
     val type: String,

    @ColumnInfo(name = "point")
     val point: Double,

    @ColumnInfo(name = "price")
     val price: Double
)