package com.vilo.search.di

import com.vilo.search.views.SearchViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureSearchModule = module {
    viewModel { SearchViewModel() }
}