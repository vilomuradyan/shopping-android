package com.vilo.shop.di

import com.vilo.shop.view.ShopViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureShopModule = module {
    viewModel { ShopViewModel() }
}
