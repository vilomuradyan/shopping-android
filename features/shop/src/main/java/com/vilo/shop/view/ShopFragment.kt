package com.vilo.shop.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vilo.common.base.BaseFragment
import com.vilo.common.base.BaseViewModel
import com.vilo.shop.databinding.FragmentShopBinding
import org.koin.android.viewmodel.ext.android.viewModel


class ShopFragment : BaseFragment() {

    private lateinit var binding : FragmentShopBinding
    private val viewModel : ShopViewModel by viewModel()

    override fun getViewModel(): BaseViewModel  = viewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShopBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

}