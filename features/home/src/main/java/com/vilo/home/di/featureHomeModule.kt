package com.vilo.home.di

import com.vilo.home.views.HomeViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureHomeModule = module {
    viewModel{HomeViewModel()}
}