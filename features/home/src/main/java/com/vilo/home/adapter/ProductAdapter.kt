package com.vilo.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vilo.home.R
import com.vilo.home.callback.HomeItemDiffCalBack
import com.vilo.home.databinding.ItemProductBinding
import com.vilo.home.views.HomeViewModel
import com.vilo.model.Product

class ProductAdapter(private val homeViewModel: HomeViewModel) :
    RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var product: MutableList<Product> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder =
        ProductViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_product, parent, false
            )
        )

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(product[position], homeViewModel)
    }

    override fun getItemCount(): Int = product.size

    fun updateData(items : List<Product>){
        val diffCallBack = HomeItemDiffCalBack(product, items)
        val diffResult = DiffUtil.calculateDiff(diffCallBack)
        product.clear()
        product.addAll(items)

        diffResult.dispatchUpdatesTo(this)
    }

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemProductBinding.bind(itemView)
        fun bind(product: Product, viewModel: HomeViewModel) {
            binding.product = product
            binding.viewModel = viewModel
        }
    }
}