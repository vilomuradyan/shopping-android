package com.vilo.home.callback

import androidx.recyclerview.widget.DiffUtil
import com.vilo.model.Product

class HomeItemDiffCalBack(
    private val oldList: List<Product>,
    private val newList: List<Product>
) : DiffUtil.Callback() {


    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].name == newList[newItemPosition].name
                && oldList[oldItemPosition].brand == newList[newItemPosition].brand
                && oldList[oldItemPosition].point == newList[newItemPosition].point
                && oldList[oldItemPosition].price == newList[newItemPosition].price
                && oldList[oldItemPosition].type  == newList[newItemPosition].type
}