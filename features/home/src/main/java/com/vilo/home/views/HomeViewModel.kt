package com.vilo.home.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vilo.common.base.BaseViewModel

class HomeViewModel : BaseViewModel() {

    private val _isKg = MutableLiveData<Boolean>()
    private val _isL = MutableLiveData<Boolean>()
    private var _isItem = MutableLiveData<Boolean>()

    val isKg : LiveData<Boolean> get() = _isKg
    val isL : LiveData<Boolean> get() = _isL
    val isItem : LiveData<Boolean> get() = _isItem

    init {
        _isKg.value = true
    }

    fun selectedKg() {
        _isKg.value = true
        _isL.value = false
        _isItem.value = false
    }

    fun selectedL() {
        _isKg.value = false
        _isL.value = true
        _isItem.value = false
    }
    fun selectedItem(){
        _isKg.value = false
        _isL.value = false
        _isItem.value = true
    }
}