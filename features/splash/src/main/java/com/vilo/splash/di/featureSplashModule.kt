package com.vilo.splash.di

import com.vilo.splash.views.SplashViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureSplashModule = module {
    viewModel{ SplashViewModel(get()) }
}