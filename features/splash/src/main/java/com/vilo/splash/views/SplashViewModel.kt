package com.vilo.splash.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.vilo.common.base.BaseViewModel
import com.vilo.repository.AppDispatchers
import com.vilo.splash.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(private val appDispatchers: AppDispatchers) : BaseViewModel() {

    fun changeActivity(activity : AppCompatActivity){
        CoroutineScope(appDispatchers.main).launch {
            delay(3000)
            activity.startActivity(Intent("com.shopping.action.intent.main"))
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            activity.finish()
        }
    }
}