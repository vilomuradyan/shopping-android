package com.vilo.history.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vilo.common.base.BaseFragment
import com.vilo.common.base.BaseViewModel
import com.vilo.history.databinding.FragmentHistoryBinding
import org.koin.android.viewmodel.ext.android.viewModel

class HistoryFragment : BaseFragment() {

    private val viewModel : HistoryViewModel by viewModel()
    private lateinit var binding : FragmentHistoryBinding

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHistoryBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }
}