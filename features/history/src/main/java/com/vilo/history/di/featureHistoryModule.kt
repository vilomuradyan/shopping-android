package com.vilo.history.di

import com.vilo.history.views.HistoryViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureHistoryModule = module {
    viewModel{ HistoryViewModel() }
}