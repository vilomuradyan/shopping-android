package com.vilo.shopping.views

import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.vilo.common.base.BaseViewModel
import com.vilo.home.views.HomeFragmentDirections
import com.vilo.repository.AppDispatchers
import com.vilo.shopping.R

class MainViewModel(private val dispatchers: AppDispatchers) : BaseViewModel(),
    BottomNavigationView.OnNavigationItemSelectedListener {


    private fun navigateShop() {
        navigate(HomeFragmentDirections.actionHomeFragmentToShopFragment())
    }
    private fun navigateHistory() {
        navigate(HomeFragmentDirections.actionHomeFragmentToHistoryFragment())
    }
    private fun navigateSearch(){
        navigate(HomeFragmentDirections.actionHomeFragmentToSearchFragment())
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shop_action -> navigateShop()
            R.id.history_action -> navigateHistory()
            R.id.search_action -> navigateSearch()
        }
        return true
    }
}