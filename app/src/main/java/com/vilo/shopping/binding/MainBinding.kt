package com.vilo.shopping.binding

import androidx.databinding.BindingAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView


object MainBinding {

    @BindingAdapter("app:onNavigationItemSelected")
    @JvmStatic
    fun setNavigationItemSelected(
        view: BottomNavigationView,
        listener: BottomNavigationView.OnNavigationItemSelectedListener?
    ) {
        view.setOnNavigationItemSelectedListener(listener)
    }
}