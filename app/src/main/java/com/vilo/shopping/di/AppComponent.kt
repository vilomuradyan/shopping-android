package com.vilo.shopping.di

import com.vilo.history.di.featureHistoryModule
import com.vilo.home.di.featureHomeModule
import com.vilo.repository.di.repositoryModule
import com.vilo.search.di.featureSearchModule
import com.vilo.shop.di.featureShopModule
import com.vilo.splash.di.featureSplashModule


val appComponent= listOf(
    repositoryModule,
    mainModule,
    featureHomeModule,
    featureShopModule,
    featureHistoryModule,
    featureSearchModule,
    featureSplashModule
)