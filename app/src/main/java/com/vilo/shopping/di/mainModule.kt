package com.vilo.shopping.di

import com.vilo.shopping.views.MainViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val mainModule = module {
    viewModel { MainViewModel(get())}
}